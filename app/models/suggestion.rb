class Suggestion < ApplicationRecord
  belongs_to :equipment
  belongs_to :store
  belongs_to :reason
end
