class Store < ApplicationRecord
  validates :name, :address, :number, presence: true

  belongs_to :route
  has_and_belongs_to_many :equipments
  has_many :suggestions
end
