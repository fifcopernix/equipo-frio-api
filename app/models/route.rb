class Route < ApplicationRecord
  validates :name, :number, presence: true

  has_many :stores
  belongs_to :supervisor
end
