class Equipment < ApplicationRecord
  validates :name, :serial, :energy_consumption,
            :size, :description, :planimetry, presence: true

  has_and_belongs_to_many :stores
  has_many :suggestions
end
