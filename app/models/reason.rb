class Reason < ApplicationRecord
  validates :text, presence: true

  has_many :suggestions
end
