ActiveAdmin.register Agent do
  permit_params :email, :password, :password_confirmation, :supervisor_id

  index do
    selectable_column
    id_column
    column :email
    column 'Supervisor' do |agent|
      Supervisor.find(agent.supervisor_id).try(:email)
    end
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Agent Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :supervisor_id, label: 'Supervisor', as: :select, collection: Supervisor.all.map { |s| [s.email, s.id] }
    end
    f.actions
  end
end
