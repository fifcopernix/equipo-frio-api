# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_171_122_221_513) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'agents', force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer 'sign_in_count', default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.inet 'current_sign_in_ip'
    t.inet 'last_sign_in_ip'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.bigint 'supervisor_id'
    t.index ['email'], name: 'index_agents_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_agents_on_reset_password_token', unique: true
    t.index ['supervisor_id'], name: 'index_agents_on_supervisor_id'
  end

  create_table 'equipment', force: :cascade do |t|
    t.string 'name'
    t.string 'serial'
    t.string 'energy_consumption'
    t.string 'size'
    t.text 'description'
    t.string 'planimetry'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'equipment_stores', force: :cascade do |t|
    t.bigint 'store_id'
    t.bigint 'equipment_id'
    t.index ['equipment_id'], name: 'index_equipment_stores_on_equipment_id'
    t.index ['store_id'], name: 'index_equipment_stores_on_store_id'
  end

  create_table 'reasons', force: :cascade do |t|
    t.string 'text'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'routes', force: :cascade do |t|
    t.string 'name'
    t.string 'number'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.bigint 'supervisor_id'
    t.index ['supervisor_id'], name: 'index_routes_on_supervisor_id'
  end

  create_table 'stores', force: :cascade do |t|
    t.string 'name'
    t.text 'address'
    t.string 'number'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.bigint 'route_id'
    t.index ['route_id'], name: 'index_stores_on_route_id'
  end

  create_table 'suggestions', force: :cascade do |t|
    t.bigint 'equipment_id'
    t.bigint 'reason_id'
    t.bigint 'store_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['equipment_id'], name: 'index_suggestions_on_equipment_id'
    t.index ['reason_id'], name: 'index_suggestions_on_reason_id'
    t.index ['store_id'], name: 'index_suggestions_on_store_id'
  end

  create_table 'supervisors', force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer 'sign_in_count', default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.inet 'current_sign_in_ip'
    t.inet 'last_sign_in_ip'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_supervisors_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_supervisors_on_reset_password_token', unique: true
  end
end
