class AddStoresToRoute < ActiveRecord::Migration[5.1]
  def change
    change_table :stores do |t|
      t.belongs_to :route, index: true
    end
  end
end
