class AddAgentsToSupervisor < ActiveRecord::Migration[5.1]
  def change
    change_table :agents do |t|
      t.belongs_to :supervisor, index: true
    end
  end
end
