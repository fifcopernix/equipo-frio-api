class CreateSuggestions < ActiveRecord::Migration[5.1]
  def change
    create_table :suggestions do |t|
      t.belongs_to :equipment, index: true
      t.belongs_to :reason, index: true
      t.belongs_to :store, inde: true

      t.timestamps
    end
  end
end
