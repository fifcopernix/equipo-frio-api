class CreateEquipment < ActiveRecord::Migration[5.1]
  def change
    create_table :equipment do |t|
      t.string :name
      t.string :serial
      t.string :energy_consumption
      t.string :size
      t.text :description
      t.string :planimetry

      t.timestamps
    end
  end
end
