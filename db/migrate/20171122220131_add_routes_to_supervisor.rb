class AddRoutesToSupervisor < ActiveRecord::Migration[5.1]
  def change
    change_table :routes do |t|
      t.belongs_to :supervisor, index: true
    end
  end
end
