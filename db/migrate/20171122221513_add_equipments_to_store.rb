class AddEquipmentsToStore < ActiveRecord::Migration[5.1]
  def change
    create_table :equipment_stores do |t|
      t.belongs_to :store, index: true
      t.belongs_to :equipment, index: true
    end
  end
end
