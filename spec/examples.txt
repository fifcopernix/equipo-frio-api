example_id                              | status | run_time        |
--------------------------------------- | ------ | --------------- |
./spec/models/agent_spec.rb[1:1:1]      | passed | 0.02574 seconds |
./spec/models/equipment_spec.rb[1:1:1]  | passed | 0.02361 seconds |
./spec/models/equipment_spec.rb[1:1:2]  | passed | 0.00701 seconds |
./spec/models/equipment_spec.rb[1:1:3]  | passed | 0.00957 seconds |
./spec/models/equipment_spec.rb[1:1:4]  | passed | 0.01195 seconds |
./spec/models/equipment_spec.rb[1:1:5]  | passed | 0.0073 seconds  |
./spec/models/equipment_spec.rb[1:1:6]  | passed | 0.01164 seconds |
./spec/models/equipment_spec.rb[1:2:1]  | passed | 0.01566 seconds |
./spec/models/equipment_spec.rb[1:2:2]  | passed | 0.01383 seconds |
./spec/models/reason_spec.rb[1:1:1]     | passed | 0.01608 seconds |
./spec/models/reason_spec.rb[1:2:1]     | passed | 0.00516 seconds |
./spec/models/route_spec.rb[1:1:1]      | passed | 0.02123 seconds |
./spec/models/route_spec.rb[1:1:2]      | passed | 0.0068 seconds  |
./spec/models/route_spec.rb[1:2:1]      | passed | 0.01391 seconds |
./spec/models/route_spec.rb[1:2:2]      | passed | 0.00523 seconds |
./spec/models/store_spec.rb[1:1:1]      | passed | 0.01623 seconds |
./spec/models/store_spec.rb[1:1:2]      | passed | 0.00716 seconds |
./spec/models/store_spec.rb[1:1:3]      | passed | 0.00718 seconds |
./spec/models/store_spec.rb[1:2:1]      | passed | 0.01628 seconds |
./spec/models/store_spec.rb[1:2:2]      | passed | 0.0039 seconds  |
./spec/models/store_spec.rb[1:2:3]      | passed | 0.00857 seconds |
./spec/models/suggestion_spec.rb[1:1:1] | passed | 0.01546 seconds |
./spec/models/suggestion_spec.rb[1:1:2] | passed | 0.0065 seconds  |
./spec/models/suggestion_spec.rb[1:1:3] | passed | 0.00658 seconds |
./spec/models/supervisor_spec.rb[1:1:1] | passed | 0.0343 seconds  |
