require 'rails_helper'

describe Equipment do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:serial) }
    it { should validate_presence_of(:energy_consumption) }
    it { should validate_presence_of(:size) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:planimetry) }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:stores) }
    it { should have_many(:suggestions) }
  end
end
