require 'rails_helper'

describe Reason do
  describe 'validations' do
    it { should validate_presence_of(:text) }
  end

  describe 'associations' do
    it { should have_many(:suggestions) }
  end
end
