require 'rails_helper'

describe Suggestion do
  describe 'associations' do
    it { should belong_to(:equipment) }
    it { should belong_to(:store) }
    it { should belong_to(:reason) }
  end
end
