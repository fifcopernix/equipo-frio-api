require 'rails_helper'

describe Store do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:address) }
    it { should validate_presence_of(:number) }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:equipments) }
    it { should belong_to(:route) }
    it { should have_many(:suggestions) }
  end
end
