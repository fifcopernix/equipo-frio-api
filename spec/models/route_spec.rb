require 'rails_helper'

describe Route do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:number) }
  end

  describe 'associations' do
    it { should have_many(:stores) }
    it { should belong_to(:supervisor) }
  end
end
