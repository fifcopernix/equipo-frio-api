require 'rails_helper'

describe Supervisor do
  describe 'associations' do
    it { should have_many(:agents) }
  end
end
