require 'rails_helper'

describe Agent do
  describe 'associations' do
    it { should belong_to(:supervisor) }
  end
end
