Rails.application.routes.draw do
  devise_for :supervisors, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :api do
  end
end
